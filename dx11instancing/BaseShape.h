#pragma once

#include "D3DDevice.h"

class BaseShape
{
	public:

		BaseShape();
		virtual ~BaseShape();

		virtual const int GetVerticesCount() const = 0;
		virtual const int GetIndicesCount() const = 0;
		virtual const SimpleVertex* GetVertices() const = 0;
		virtual const WORD* GetIndices() const = 0;
};

