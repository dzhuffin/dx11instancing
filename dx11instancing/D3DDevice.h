#pragma once

#include "stdafx.h"
#include "Camera.h"

struct SimpleVertex
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT4 color;
};

struct InstanceData
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT4 rotation;
};

struct ConstantBuffer
{
	DirectX::XMMATRIX mWorld;
	DirectX::XMMATRIX mView;
	DirectX::XMMATRIX mProjection;
	UINT countPerRing;
	float ringRadius;
	float ringOffset;
	float dt;
	float time;
};

class D3DDevice
{
	private:

		D3D_DRIVER_TYPE         driverType = D3D_DRIVER_TYPE_NULL;
		D3D_FEATURE_LEVEL       featureLevel = D3D_FEATURE_LEVEL_11_0;
		ID3D11Device*           pd3dDevice = nullptr;
		ID3D11Device1*          pd3dDevice1 = nullptr;
		ID3D11DeviceContext*    pImmediateContext = nullptr;
		ID3D11DeviceContext1*   pImmediateContext1 = nullptr;
		IDXGISwapChain*         pSwapChain = nullptr;
		IDXGISwapChain1*        pSwapChain1 = nullptr;
		ID3D11RenderTargetView* pRenderTargetView = nullptr;
		ID3D11DepthStencilView* pDepthStencilView;
		ID3D11Texture2D*		pDepthStencilBuffer;
		ID3D11VertexShader*     pVertexShader = nullptr;
		ID3D11PixelShader*      pPixelShader = nullptr;
		ID3D11InputLayout*      pVertexLayout = nullptr;
		ID3D11Buffer*           pVertexBuffer = nullptr;
		ID3D11Buffer*           pIndexBuffer = nullptr;
		ID3D11Buffer*           pInstanceBuffer = nullptr;
		ID3D11Buffer*           pConstantBuffer = nullptr;
		DirectX::XMMATRIX       worldMatrix;
		DirectX::XMMATRIX       projectionMatrix;

		const int RingsCount = 32;

		ConstantBuffer constantBufferData;

		std::vector<SimpleVertex> vertTempStorage;
		std::vector<WORD> idxTempStorage;

		void GeneratePath(std::vector<InstanceData> &inst, int numInst) const;

	public:

		D3DDevice();
		virtual ~D3DDevice();

		HRESULT Init(HINSTANCE hInst, HWND hWnd);
		void CleanUp();

		void Render(const Camera& camera, float dt);

		void AddVerticesToStorage(const SimpleVertex *vertexPtr, int count);
		void AddIndexesToStorage(const WORD *indexPtr, int count);
		HRESULT InitBuffersForShapes();
};