#pragma once

#include "CubeShape.h"
#include "Camera.h"

class D3DDevice;
class DXInput;
class Spline;

class Game
{
	private:

		D3DDevice d3dDevice;
		DXInput dxInput;
		CubeShape cubeShape;
		Camera camera;

	public:

		Game();
		virtual ~Game();

		HRESULT Init(HINSTANCE hInst, HWND hWnd);
		bool Update(float dt);
};

