//--------------------------------------------------------------------------------------
cbuffer ConstantBuffer : register( b0 )
{
	matrix world;
	matrix view;
	matrix projection;
	uint countPerRing;
	float ringRadius;
	float ringOffset;
	float dt;
	float time;
}

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 pos : SV_POSITION;
    float4 color : COLOR0;
};

//--------------------------------------------------------------------------------------
float3 rotate_vector(float4 q, float3 v)
{
	float3 t = 2.0f * cross(q.xyz, v);
	return v + q.w * t + cross(q.xyz, t);
}

//--------------------------------------------------------------------------------------
VS_OUTPUT VS(float4 pos : POSITION, float4 color : COLOR, float3 instancePos : INSTANCEPOS, 
	float4 rotation : INSTANCEROTATION, uint instanceId : SV_InstanceID)
{
	float angleStep = 3.14159265f * 2.0f / countPerRing;

	float angle = instanceId % countPerRing * angleStep;

	pos += float4(ringRadius * cos(angle), -ringRadius * sin(angle), 0.0f, 0.0f);
	
	pos += float4(rotate_vector(rotation, float3(pos.x, pos.y, pos.z)), 0.0f);

	pos += float4(instancePos, 0.0f);

    VS_OUTPUT output = (VS_OUTPUT)0;
    output.pos = mul( pos, world );
    output.pos = mul( output.pos, view );
    output.pos = mul( output.pos, projection );

	uint ringNum = uint(instanceId / countPerRing);
	output.color = color * (ringNum % 5 + 1) * 0.08f;

    return output;
}


//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
    return input.color;
}
