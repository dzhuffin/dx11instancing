#include "stdafx.h"
#include "DXInput.h"
#include "Camera.h"

using namespace DirectX;

//---------------------------------------------------------------------------------------------------------
Camera::Camera() : position(DirectX::XMVectorSet(0.0f, 0.0f, -5.0f, 0.0f))
{
}


//---------------------------------------------------------------------------------------------------------
Camera::~Camera()
{
}


//---------------------------------------------------------------------------------------------------------
void Camera::Update(float dt, const DXInput& input)
{
	const float factorView = 0.001f;
	const float factorSpeed = 50.0f;

	moveLeftRight = 0.0f;
	moveBackForward = 0.0f;

	if (input.IsKeyPressed(DIK_W))
		moveBackForward += dt * factorSpeed;
	if (input.IsKeyPressed(DIK_S))
		moveBackForward -= dt * factorSpeed;
	if (input.IsKeyPressed(DIK_D))
		moveLeftRight += dt * factorSpeed;
	if (input.IsKeyPressed(DIK_A))
		moveLeftRight -= dt * factorSpeed;

	pitch += input.GetMouseDy() * factorView;
	yaw += input.GetMouseDx() * factorView;

	rotationMatrix = XMMatrixRotationRollPitchYaw(pitch, yaw, 0.0f);
	auto targetVector = XMVector3TransformCoord(DefaultForward, rotationMatrix);
	targetVector = XMVector3Normalize(targetVector);

	XMMATRIX rotationMoveMatrix = XMMatrixRotationY(yaw);

	position += moveLeftRight * XMVector3TransformCoord(DefaultRight, rotationMoveMatrix);
	position += moveBackForward * XMVector3TransformCoord(DefaultForward, rotationMoveMatrix);

	targetVector += position;

	viewMatrix = XMMatrixLookAtLH(position, targetVector, DefaultUp);
}


//---------------------------------------------------------------------------------------------------------
const XMMATRIX& Camera::GetViewMatrix() const
{
	return viewMatrix;
}