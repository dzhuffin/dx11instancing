#pragma once

#include "stdafx.h"

class DXInput
{
	private:

		LPDIRECTINPUT8          pDI = nullptr;
		LPDIRECTINPUTDEVICE8    pMouse = nullptr;
		LPDIRECTINPUTDEVICE8    pKeyboard = nullptr;

		DIMOUSESTATE mouseState;
		DIMOUSESTATE prevMouseState;
		BYTE keyboardState[256];

	public:

		DXInput();
		virtual ~DXInput();

		HRESULT DXInput::Init(HWND hDlg);
		void CleanUp();

		void Update();

		bool IsKeyPressed(int keyCode) const;
		bool IsMouseButtonPressed(int button) const;

		FORCEINLINE float GetMouseDx() const { return (float)mouseState.lX; };
		FORCEINLINE float GetMouseDy() const { return (float)mouseState.lY; };
		FORCEINLINE float GetMouseDz() const { return (float)mouseState.lZ; };
};

