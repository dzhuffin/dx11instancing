#include "stdafx.h"
#include "Spline.h"

Spline::Spline()
	: vp(), delta_t(0)
{
}

Spline::Spline(const Spline& s)
{
    for (int i = 0; i < (int)s.vp.size(); i++)
        vp.push_back(s.vp[i]);
    delta_t = s.delta_t;
}

Spline::Spline(const std::vector<vec3>& points)
{
	for (int i = 0; i < (int)points.size(); i++)
		vp.push_back(points[i]);
	delta_t = (float)1 / (float)(points.size() - 1);
}

Spline::~Spline()
{}

// Solve the Catmull-Rom parametric equation for a given time(t) and vector quadruple (p1,p2,p3,p4)
vec3 Spline::Equation(float t, const vec3& p1, const vec3& p2, const vec3& p3, const vec3& p4)
{
    float t2 = t * t;
    float t3 = t2 * t;

    float b1 = 0.5f * (       -t3 + 2.0f * t2 - t   );
    float b2 = 0.5f * ( 3.0f * t3 - 5.0f * t2 + 2.0f);
    float b3 = 0.5f * (-3.0f * t3 + 4.0f * t2 + t   );
    float b4 = 0.5f * (        t3 -        t2       );

    return (p1 * b1 + p2 * b2 + p3 * b3 + p4 * b4); 
}

vec3 Spline::DerivativeEquation(float t, const vec3& p1, const vec3& p2, const vec3& p3, const vec3& p4)
{
	float t2 = t * t;

	float b1 = -1.5f * t2 + 2.0f * t - 0.5f;
	float b2 =  4.5f * t2 - 5.0f * t;
	float b3 = -4.5f * t2 + 4.0f * t + 0.5f;
	float b4 =  1.5f * t2 -        t;

	return (p1 * b1 + p2 * b2 + p3 * b3 + p4 * b4);
}

void Spline::CalcInterpolatedSpline(float t, vec3& point, vec3& derivative)
{
    // Find out in which interval we are on the spline
    int p = (int)(t / delta_t);
    // Compute local control point indices
#define BOUNDS(pp, pMax) { if (pp < 0) pp = 0; else if (pp >= pMax) pp = pMax; }
    int p0 = p - 1;     BOUNDS(p0, vp.size() - 3);
    int p1 = p;         BOUNDS(p1, vp.size() - 2);
    int p2 = p + 1;     BOUNDS(p2, vp.size() - 1);
    int p3 = p + 2;     BOUNDS(p3, vp.size() - 1);
    // Relative (local) time 
	float lt = (t - delta_t*(float)p1) / delta_t;
	// Interpolate
	point = Spline::Equation(lt, vp[p0], vp[p1], vp[p2], vp[p3]);
	derivative = Spline::DerivativeEquation(lt, vp[p0], vp[p1], vp[p2], vp[p3]);
}

int Spline::GetNumPoints()
{
	return vp.size();
}

vec3& Spline::GetNthPoint(int n)
{
	return vp[n];
}