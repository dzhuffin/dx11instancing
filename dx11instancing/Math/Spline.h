#pragma once

#include "vec3.h"
#include <vector>

class Spline
{
public:

    Spline();
    Spline(const Spline&);
	Spline(const std::vector<vec3>& points);
    ~Spline();

	void CalcInterpolatedSpline(float t, vec3& point, vec3& derivative);
	int GetNumPoints();
	vec3& GetNthPoint(int n);

    // Static method for computing the Catmull-Rom parametric equation
    // given a time (t) and a vector quadruple (p1,p2,p3,p4).
    static vec3 Equation(float t, const vec3& p1, const vec3& p2, const vec3& p3, const vec3& p4);
	static vec3 DerivativeEquation(float t, const vec3& p1, const vec3& p2, const vec3& p3, const vec3& p4);

private:

    std::vector<vec3> vp;
    float delta_t;
};
