#include "stdafx.h"
#include "DXInput.h"
#include <iostream>
#include <sstream>
#include "Game.h"

using namespace DirectX;

//---------------------------------------------------------------------------------------------------------
Game::Game()
{
}


//---------------------------------------------------------------------------------------------------------
Game::~Game()
{
}


//---------------------------------------------------------------------------------------------------------
HRESULT Game::Init(HINSTANCE hInst, HWND hWnd)
{
	if (FAILED(d3dDevice.Init(hInst, hWnd)))
		return S_FALSE;

	if (FAILED(dxInput.Init(hWnd)))
		return S_FALSE;

	d3dDevice.AddVerticesToStorage(cubeShape.GetVertices(), cubeShape.GetVerticesCount());
	d3dDevice.AddIndexesToStorage(cubeShape.GetIndices(), cubeShape.GetIndicesCount());

	if (FAILED(d3dDevice.InitBuffersForShapes()))
		return S_FALSE;

	return S_OK;
}

//---------------------------------------------------------------------------------------------------------
bool Game::Update(float dt)
{
	dxInput.Update();

	if (dxInput.IsKeyPressed(DIK_ESCAPE))
		return false;

	camera.Update(dt, dxInput);
	d3dDevice.Render(camera, dt);

	return true;
}