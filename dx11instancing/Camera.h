#pragma once

class DXInput;

class Camera
{
	private:

		const DirectX::XMVECTOR DefaultForward = DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
		const DirectX::XMVECTOR DefaultRight = DirectX::XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);
		const DirectX::XMVECTOR DefaultUp = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

		DirectX::XMVECTOR position;

		DirectX::XMMATRIX rotationMatrix;
		DirectX::XMMATRIX viewMatrix;

		float moveLeftRight = 0.0f;
		float moveBackForward = 0.0f;

		float yaw = 0.0f;
		float pitch = 0.0f;

	public:

		Camera();
		virtual ~Camera();

		const DirectX::XMMATRIX& GetViewMatrix() const;
		void Update(float dt, const DXInput& input);
};

