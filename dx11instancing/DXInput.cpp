#include "stdafx.h"
#include "DXInput.h"


//---------------------------------------------------------------------------------------------------------
DXInput::DXInput()
{
}


//---------------------------------------------------------------------------------------------------------
DXInput::~DXInput()
{
	CleanUp();
}


//---------------------------------------------------------------------------------------------------------
HRESULT DXInput::Init(HWND hDlg)
{
	HRESULT hr;

	if (FAILED(hr = DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&pDI, nullptr)))
		return hr;

	if (FAILED(pDI->CreateDevice(GUID_SysKeyboard, &pKeyboard, nullptr)))
	{
		MessageBox(nullptr, TEXT("Keyboard not found. The sample will now exit."),
			TEXT("DirectInput Sample"),
			MB_ICONERROR | MB_OK);
		EndDialog(hDlg, 0);
		return S_OK;
	}

	if (FAILED(hr = pKeyboard->SetDataFormat(&c_dfDIKeyboard)))
		return hr;

	if (FAILED(hr = pKeyboard->SetCooperativeLevel(hDlg, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND)))
		return hr;

	if (FAILED(pDI->CreateDevice(GUID_SysMouse, &pMouse, nullptr)))
	{
		MessageBox(nullptr, TEXT("Mouse not found. The sample will now exit."),
			TEXT("DirectInput Sample"),
			MB_ICONERROR | MB_OK);
		EndDialog(hDlg, 0);
		return S_OK;
	}

	if (FAILED(hr = pMouse->SetDataFormat(&c_dfDIMouse)))
		return hr;

	if (FAILED(hr = pMouse->SetCooperativeLevel(hDlg, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND)))
		return hr;

	return S_OK;
}


//---------------------------------------------------------------------------------------------------------
void DXInput::CleanUp()
{
	if (pKeyboard)
		pKeyboard->Unacquire();

	if (pMouse)
		pMouse->Unacquire();

	SAFE_RELEASE(pKeyboard);
	SAFE_RELEASE(pMouse);
	SAFE_RELEASE(pDI);
}


//---------------------------------------------------------------------------------------------------------
void DXInput::Update()
{
	pKeyboard->Acquire();
	pKeyboard->GetDeviceState(sizeof(keyboardState), (LPVOID)&keyboardState);

	pMouse->Acquire();
	pMouse->GetDeviceState(sizeof(DIMOUSESTATE), &mouseState);
}


//---------------------------------------------------------------------------------------------------------
bool DXInput::IsKeyPressed(int keyCode) const
{
	return (keyboardState[keyCode] & 0x80) > 0;
}


//---------------------------------------------------------------------------------------------------------
bool DXInput::IsMouseButtonPressed(int button) const
{
	return (mouseState.rgbButtons[button] & 0x80) > 0;
}