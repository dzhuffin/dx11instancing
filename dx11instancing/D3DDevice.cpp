#include "stdafx.h"
#include "D3DDevice.h"
#include "BaseShape.h"
#include "Camera.h"
#include "math/Spline.h"

using namespace DirectX;


//---------------------------------------------------------------------------------------------------------
D3DDevice::D3DDevice()
{
	constantBufferData.countPerRing = 24;
	constantBufferData.ringRadius = 10.0f;
	constantBufferData.ringOffset = 3.0f;
	constantBufferData.time = 0.0f;
}


//---------------------------------------------------------------------------------------------------------
D3DDevice::~D3DDevice()
{
	CleanUp();
}


//---------------------------------------------------------------------------------------------------------
void D3DDevice::CleanUp()
{
	SAFE_RELEASE(pDepthStencilView);
	SAFE_RELEASE(pDepthStencilBuffer);
	SAFE_RELEASE(pImmediateContext)
	SAFE_RELEASE(pConstantBuffer)
	SAFE_RELEASE(pVertexBuffer)
	SAFE_RELEASE(pIndexBuffer)
	SAFE_RELEASE(pInstanceBuffer)
	SAFE_RELEASE(pVertexLayout)
	SAFE_RELEASE(pVertexShader)
	SAFE_RELEASE(pPixelShader)
	SAFE_RELEASE(pRenderTargetView)
	SAFE_RELEASE(pSwapChain1)
	SAFE_RELEASE(pSwapChain)
	SAFE_RELEASE(pImmediateContext1)
	SAFE_RELEASE(pImmediateContext)
	SAFE_RELEASE(pd3dDevice1)
	SAFE_RELEASE(pd3dDevice)
}


//---------------------------------------------------------------------------------------------------------
HRESULT D3DDevice::Init(HINSTANCE hInst, HWND hWnd)
{
	HRESULT hr = S_OK;

	RECT rc;
	GetClientRect(hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	UINT createDeviceFlags = 0;

#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; ++driverTypeIndex)
	{
		driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDevice(nullptr, driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &pd3dDevice, &featureLevel, &pImmediateContext);

		if (hr == E_INVALIDARG)
		{
			// DirectX 11.0 platforms will not recognize D3D_FEATURE_LEVEL_11_1 so we need to retry without it
			hr = D3D11CreateDevice(nullptr, driverType, nullptr, createDeviceFlags, &featureLevels[1], numFeatureLevels - 1,
				D3D11_SDK_VERSION, &pd3dDevice, &featureLevel, &pImmediateContext);
		}

		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
		return hr;

	// Obtain DXGI factory from device (since we used nullptr for pAdapter above)
	IDXGIFactory1* dxgiFactory = nullptr;
	{
		IDXGIDevice* dxgiDevice = nullptr;
		hr = pd3dDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice));
		if (SUCCEEDED(hr))
		{
			IDXGIAdapter* adapter = nullptr;
			hr = dxgiDevice->GetAdapter(&adapter);
			if (SUCCEEDED(hr))
			{
				hr = adapter->GetParent(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&dxgiFactory));
				adapter->Release();
			}
			dxgiDevice->Release();
		}
	}
	if (FAILED(hr))
		return hr;

	// Create swap chain
	IDXGIFactory2* dxgiFactory2 = nullptr;
	hr = dxgiFactory->QueryInterface(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxgiFactory2));
	if (dxgiFactory2)
	{
		// DirectX 11.1 or later
		hr = pd3dDevice->QueryInterface(__uuidof(ID3D11Device1), reinterpret_cast<void**>(&pd3dDevice1));
		if (SUCCEEDED(hr))
		{
			(void)pImmediateContext->QueryInterface(__uuidof(ID3D11DeviceContext1), reinterpret_cast<void**>(&pImmediateContext1));
		}

		DXGI_SWAP_CHAIN_DESC1 sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.Width = width;
		sd.Height = height;
		sd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.BufferCount = 1;

		hr = dxgiFactory2->CreateSwapChainForHwnd(pd3dDevice, hWnd, &sd, nullptr, nullptr, &pSwapChain1);
		if (SUCCEEDED(hr))
		{
			hr = pSwapChain1->QueryInterface(__uuidof(IDXGISwapChain), reinterpret_cast<void**>(&pSwapChain));
		}

		dxgiFactory2->Release();
	}
	else
	{
		// DirectX 11.0 systems
		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = width;
		sd.BufferDesc.Height = height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = hWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;

		hr = dxgiFactory->CreateSwapChain(pd3dDevice, &sd, &pSwapChain);
	}

	// Note this tutorial doesn't handle full-screen swapchains so we block the ALT+ENTER shortcut
	dxgiFactory->MakeWindowAssociation(hWnd, DXGI_MWA_NO_ALT_ENTER);

	dxgiFactory->Release();

	if (FAILED(hr))
		return hr;

	// Create a render target view
	ID3D11Texture2D* pBackBuffer = nullptr;
	hr = pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer));
	if (FAILED(hr))
		return hr;

	hr = pd3dDevice->CreateRenderTargetView(pBackBuffer, nullptr, &pRenderTargetView);
	pBackBuffer->Release();
	if (FAILED(hr))
		return hr;

	pImmediateContext->OMSetRenderTargets(1, &pRenderTargetView, nullptr);

	// Setup the viewport
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)width;
	vp.Height = (FLOAT)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	pImmediateContext->RSSetViewports(1, &vp);

	//Describe our Depth/Stencil Buffer
	D3D11_TEXTURE2D_DESC depthStencilDesc;

	depthStencilDesc.Width = width;
	depthStencilDesc.Height = height;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	//Create the Depth/Stencil View
	pd3dDevice->CreateTexture2D(&depthStencilDesc, NULL, &pDepthStencilBuffer);
	pd3dDevice->CreateDepthStencilView(pDepthStencilBuffer, NULL, &pDepthStencilView);

	//Set our Render Target
	pImmediateContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);

	// Compile the vertex shader
	ID3DBlob* pVSBlob = nullptr;
	hr = D3DReadFileToBlob(L"dx11instancing_VS.cso", &pVSBlob);
	if (FAILED(hr))
	{
		if (pVSBlob)
			pVSBlob->Release();

		MessageBox(nullptr, L"Vertex shader file cannot be loaded. Please run this executable from the directory that contains the CSO file.", L"Error", MB_OK);
		return hr;
	}

	// Create the vertex shader
	hr = pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), nullptr, &pVertexShader);
	if (FAILED(hr))
	{
		pVSBlob->Release();
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEROTATION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), &pVertexLayout);
	pVSBlob->Release();
	if (FAILED(hr))
		return hr;

	// Set the input layout
	pImmediateContext->IASetInputLayout(pVertexLayout);

	// Compile the vertex shader
	ID3DBlob* pPSBlob = nullptr;
	hr = D3DReadFileToBlob(L"dx11instancing_PS.cso", &pPSBlob);
	if (FAILED(hr))
	{
		if (pPSBlob)
			pPSBlob->Release();

		MessageBox(nullptr, L"Pixel shader file cannot be loaded. Please run this executable from the directory that contains the CSO file.", L"Error", MB_OK);
		return hr;
	}

	// Create the pixel shader
	hr = pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), nullptr, &pPixelShader);
	pPSBlob->Release();
	if (FAILED(hr))
		return hr;

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = pd3dDevice->CreateBuffer(&bd, nullptr, &pConstantBuffer);
	if (FAILED(hr))
		return hr;

	// Set primitive topology
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Initialize the world matrix
	worldMatrix = XMMatrixIdentity();

	// Initialize the projection matrix
	projectionMatrix = XMMatrixPerspectiveFovLH(XM_PIDIV2, width / (float)height, 0.01f, 1000.0f);

	return S_OK;
}


//---------------------------------------------------------------------------------------------------------
void D3DDevice::Render(const Camera& camera, float dt)
{
	pImmediateContext->ClearRenderTargetView(pRenderTargetView, Colors::MidnightBlue);

	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	constantBufferData.mWorld = XMMatrixTranspose(worldMatrix);
	constantBufferData.mView = XMMatrixTranspose(camera.GetViewMatrix());
	constantBufferData.mProjection = XMMatrixTranspose(projectionMatrix);
	constantBufferData.dt = dt;
	constantBufferData.time += dt;
	pImmediateContext->UpdateSubresource(pConstantBuffer, 0, nullptr, &constantBufferData, 0, 0);

	pImmediateContext->VSSetShader(pVertexShader, nullptr, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
	pImmediateContext->PSSetShader(pPixelShader, nullptr, 0);
	pImmediateContext->DrawIndexedInstanced(36, constantBufferData.countPerRing * RingsCount, 0, 0, 0);

	pSwapChain->Present(0, 0);
}


//---------------------------------------------------------------------------------------------------------
void D3DDevice::AddVerticesToStorage(const SimpleVertex *vertexPtr, int count)
{
	if (count <= 0)
		return;

	vertTempStorage.resize(vertTempStorage.size() + count);
	std::copy(vertexPtr, vertexPtr + count, vertTempStorage.end() - count);
}


//---------------------------------------------------------------------------------------------------------
void D3DDevice::AddIndexesToStorage(const WORD *indexPtr, int count)
{
	if (count <= 0)
		return;

	idxTempStorage.resize(idxTempStorage.size() + count);
	std::copy(indexPtr, indexPtr + count, idxTempStorage.end() - count);
}


//---------------------------------------------------------------------------------------------------------
void D3DDevice::GeneratePath(std::vector<InstanceData> &inst, int numInst) const
{
	std::vector<vec3> points = { vec3(0.0f, -30.0f, 0.0f),
								 vec3(0.0f, 30.0f, 60.0f),
							     vec3(0.0f, -30.0f, 120.0f),
							     vec3(0.0f, 30.0f, 180.0f),
							     vec3(0.0f, -30.0f, 240.0f),
								 vec3(0.0f, 30.0f, 300.0f)
							   };

	Spline spline(points);

	XMVECTOR tempPos;

	const XMVECTOR forward = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);

	int instanceIndex = 0;
	for (int i = 0; i < RingsCount; ++i)
	{
		float t = (float)i / (float)(RingsCount - 1);
		vec3 point;
		vec3 derivative;
		spline.CalcInterpolatedSpline(t, point, derivative);

		for (UINT j = 0; j < constantBufferData.countPerRing; ++j, ++instanceIndex)
		{
			tempPos = XMVectorSet(point.x, point.y, point.z, 0.0f);
			XMStoreFloat3(&inst[instanceIndex].pos, tempPos);

			tempPos = XMVector3Normalize(XMVectorSet(derivative.x, derivative.y, derivative.z, 0.0f));
			XMVECTOR rotationAxis = XMVector3Cross(forward, tempPos);
			XMVECTOR dot = XMVector3Dot(forward, tempPos);
			XMVECTOR length = XMVector3Length(rotationAxis);
			float rotationAngle = (float)atan2(length.m128_f32[0], dot.m128_f32[0]);

			XMVECTOR rotationQuaternion = XMQuaternionRotationNormal(XMVector3Normalize(rotationAxis), rotationAngle);

			XMStoreFloat4(&inst[instanceIndex].rotation, rotationQuaternion);
		}
	}
}


//---------------------------------------------------------------------------------------------------------
HRESULT D3DDevice::InitBuffersForShapes()
{
	if (vertTempStorage.size() <= 0 || idxTempStorage.size() <= 0)
		return S_FALSE;

	HRESULT hr = S_OK;

	// vertex buffer

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * vertTempStorage.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertTempStorage.data();
	hr = pd3dDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer);
	if (FAILED(hr))
		return hr;

	// index buffer

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * idxTempStorage.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = idxTempStorage.data();
	hr = pd3dDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer);
	if (FAILED(hr))
		return hr;

	pImmediateContext->IASetIndexBuffer(pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	const int numInst = constantBufferData.countPerRing * RingsCount;

	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(InstanceData) * numInst;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA instData;
	ZeroMemory(&instData, sizeof(instData));

	// instance buffer

	std::vector<InstanceData> inst(numInst);
	GeneratePath(inst, numInst);
	instData.pSysMem = &inst[0];

	hr = pd3dDevice->CreateBuffer(&bd, &instData, &pInstanceBuffer);

	// set vertex and instance buffers

	ID3D11Buffer* vertInstBuffers[2] = { pVertexBuffer, pInstanceBuffer };

	UINT strides[2] = { sizeof(SimpleVertex), sizeof(InstanceData) };
	UINT offsets[2] = { 0, 0 };
	pImmediateContext->IASetVertexBuffers(0, 2, vertInstBuffers, strides, offsets);

	// clear memory

	vertTempStorage.clear();
	vertTempStorage.shrink_to_fit();
	idxTempStorage.clear();
	idxTempStorage.shrink_to_fit();

	return S_OK;
}