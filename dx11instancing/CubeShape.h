#pragma once

#include "BaseShape.h"

class CubeShape : public BaseShape
{
	private:

		static const SimpleVertex vertices[8];
		static const WORD indices[36];

	public:

		CubeShape();
		virtual ~CubeShape();

		virtual const int GetVerticesCount() const;
		virtual const int GetIndicesCount() const;
		virtual const SimpleVertex* GetVertices() const;
		virtual const WORD* GetIndices() const;
};

